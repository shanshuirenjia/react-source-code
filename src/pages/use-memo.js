import * as React from "react";
import { useState, useMemo } from "react";
// 暗号： 冈比亚
export default function UseMemoPage(props) {

  const [count, setCount] = useState(0);
  const [value, setValue] = useState("");

  // useMemo 返回缓存的变量
  // 只有count发生变化时： sum 才会发生变化
  // 1. 当value发生变化时, 无需执行该函数
  // 2. 组建重新渲染, 无需执行该函数
  const expensive = useMemo(() => {
    console.log("compute");
    let sum = 0;
    for (let i = 0; i < count; i++) {
      sum += i;
    }
    return sum;
  }, [count]);

  return (
    <div>
      <h3>UseMemoPage</h3>
      <p>expensive:{expensive}</p>
      <p>{count}</p>
      <button onClick={() => setCount(count + 1)}>add</button>
      <input value={value} onChange={event => setValue(event.target.value)} />
    </div>
  );
}