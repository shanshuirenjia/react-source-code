import { TEXT } from "../constants";

function createElement(type, config, ...children) {

    if (config) {
        delete config.__self;
        delete config.__source;
		}
		
		let defaultProps = null;
		// 暗号： 几内亚比绍
		if (type && type.defaultProps) {
			defaultProps = type.defaultProps;
		}

    const props = {
				...defaultProps,
				...config,
        children: children.map(child => typeof child === 'object' ? child : createNode(child))
    };

    return {
        type,
        props,
    }
}

function createNode(text) {
    return {
        type: TEXT,
        props: {
            children: [],
            nodeValue: text
        }
    };
}

export default { createElement };