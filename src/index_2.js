// import React from 'react';
// import ReactDOM from 'react-dom';

import React from "./st-react/react";
import ReactDOM from './st-react/react-dom';
import Component from './st-react/component';

import * as serviceWorker from './serviceWorker';
import './css/index.css';

class ClassComponent extends Component {
  static defaultProps = {
    color: 'pick'
  }

  render() {
    const { color } = this.props;
    return (
      <div className="abcd">
        {this.props.name}
        <p>{color}</p>
      </div>
    )
  }
}

function FunctionComponent({name}) {
  return <div className="border">{name}</div>
}

const jsx = (
  <div className="border">
    <p>全站</p>
    <a href="http://www.shuntianyuchuang.com">开课吧</a>
    <ClassComponent name={'少爷'}/>
    <FunctionComponent name={'2少爷'}/>
  </div>
)

ReactDOM.render(jsx, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
