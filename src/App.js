import React from 'react';

function App() {
  return (
    <div className="App">
      <header className="App-header">react source code learning</header>
    </div>
  );
}

export default App;