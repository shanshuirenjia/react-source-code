import * as React from "react";
import { useState, useCallback, PureComponent } from "react";

// 暗号： 冈比亚
export default function UseCallbackPage(props) {
  const [count, setCount] = useState(0);
  
  // useCallback 返回的时缓存的函数
  // addClick 依赖变量 count， count变更时会返回新的函数， val变更时， 无需返回新的函数
  // 可以使用useCallback 优化性能
  const addClick = useCallback(() => {
    let sum = 0;
    for (let i = 0; i < count; i++) {
      sum += i;
    }
    return sum;
  }, [count]);
  
  const [value, setValue] = useState("");
  return (
    <div>
      <h3>UseCallbackPage</h3>
      <p>{count}</p>
      <button onClick={() => setCount(count + 1)}>add</button>
      <input value={value} onChange={event => setValue(event.target.value)} />
      <Child addClick={addClick} />
    </div>
  );
}

class Child extends PureComponent {
  render() {
    console.log("child render");
    const { addClick } = this.props;
    return (
      <div>
        <h3>Child</h3>
        <button onClick={() => console.log(addClick())}>add</button>
      </div>
    );
  }
}